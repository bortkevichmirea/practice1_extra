import sys
import json

def merge_unique(arr1, arr2):
    set1 = set(arr1)
    set2 = set(arr2)
    unique_elements = list(set1 & set2)
    return unique_elements

if __name__ == "__main__":
    array1_str = sys.argv[1]
    array2_str = sys.argv[2]
    
    array1 = json.loads(array1_str)
    array2 = json.loads(array2_str)
    
    result = merge_unique(array1, array2)
    
    print("Исходный массив 1:", array1)
    print("Исходный массив 2:", array2)
    print("Уникальные элементы в обоих массивах:", result)
